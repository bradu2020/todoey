//
//  Item.swift
//  Todoey
//
//  Created by Radu Baloi on 09/01/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift


//Object is a superclass that defines REALM MODEL OBJECTS
//-> need to mark variables with @objc dynamic (declaration modifier, tells runtime to use DYNAMIC dispatch
//over the standard STATIC dispatch, allows properties to be MONITOR for CHANGES at RUNTIME
class Item: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var dateCreated: Date?
    //Realm class - inverse of List<Item>
    //each item has a parent category,type Category, comes from property items in Category class
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
