//
//  AppDelegate.swift
//  Destini
//
//  Created by Philipp Muellauer on 01/09/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //locate where realm database is stored //finder -> cmd shift g
        //print(Realm.Configuration.defaultConfiguration.fileURL)
        
        //method that throws -do try,catch
        do {
            _ = try Realm() //runs once

        } catch {
            print("ERROR init new realm \(error)")
        }
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext() //for core data
    }

// MARK: - Core Data stack

lazy var persistentContainer: NSPersistentContainer = {

    let container = NSPersistentContainer(name: "DataModel")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
            
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    })
    return container
}()  //lazy -> gets a value when persistentContainer is used and this code will run()

// MARK: - Core Data Saving support

func saveContext () {
    //change data then save it from temporary to permanent storage
    let context = persistentContainer.viewContext
    if context.hasChanges {
        do {
            try context.save() //commit
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
     }
  }
}


