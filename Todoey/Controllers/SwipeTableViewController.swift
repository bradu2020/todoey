//
//  SwipeViewController.swift
//  Todoey
//
//  Created by Radu Baloi on 10/01/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import UIKit
import SwipeCellKit

//storyboard -> itodo,category cell -> cls SwipeTableViewCell module SwipeCellKit
// both cells must have identifier "Cell"

//Superclass MUST NOT know about subclasses
class SwipeTableViewController: UITableViewController, SwipeTableViewCellDelegate {

    //init swipe table view cell as the default cell for all TableView that inherit this class
    var cell: UITableViewCell?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //increase size so swipe icons fit
        tableView.rowHeight = 80.0
    }
    
    //MARK: - TableView Data Source methods
    
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SwipeTableViewCell
            
            cell.delegate = self //Enable this class delegate methods on Cell
            
            return cell
        }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
            guard orientation == .right else { return nil }

            let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
                
                self.updateModel(at: indexPath)

            }
                        deleteAction.image = UIImage(named: "delete-icon")

                        return [deleteAction]
                    }
        
        func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
            var options = SwipeOptions()
            options.expansionStyle = .destructive
            options.transitionStyle = .reveal
            return options
        }

    func updateModel(at indexPath: IndexPath) {
        //override from subClass
        //code here runs only if subClass calls super.updateModel(at: indexPath)
        
    }
}

