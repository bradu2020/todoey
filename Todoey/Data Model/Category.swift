//
//  Category.swift
//  Todoey
//
//  Created by Radu Baloi on 09/01/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation
import RealmSwift


//Object is a superclass that defines REALM MODEL OBJECTS
//-> need to mark variables with @objc dynamic (declaration modifier, tells runtime to use DYNAMIC dispatch
//over the standard STATIC dispatch, allows properties to be MONITOR for CHANGES at RUNTIME
class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var color: String?
    //collection type from Realm,similar let array = Array<Int>() to init empty arr of ints
    let items = List<Item>() // one category - many items

}
