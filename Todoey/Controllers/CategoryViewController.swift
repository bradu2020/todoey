//
//  CategoryViewController.swift
//  Todoey
//
//  Created by Radu Baloi on 09/01/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class CategoryViewController: SwipeTableViewController{
    
    let realm = try! Realm() //can throw only the first time a realm instance is created on a given threadif there is no memory
    
    //AUTO-UPDATING CONTAINER TYPE (RealmSwift) returned from Object queries
    var categories: Results<Category>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadCategories()
        
        //remove separators
        tableView.separatorStyle = .none
        
    }
    
    //keep category nav bar color blueß
    override func viewWillAppear(_ animated: Bool) {
        guard let navBar =  navigationController?.navigationBar else {
            fatalError("Navigation bar does not exist.")
        }
        navBar.backgroundColor = UIColor(hexString: "1D9BF6")
    }
    
    //MARK: - TableView DataSource methods
    
    //create as many cells as items in arr
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //nil coalescing operator
        return categories?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //access cell from super class //at the same tableVier and index path
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        //modify the swipe cell
        if let category = categories?[indexPath.row] {
            cell.textLabel?.text = category.name
            
            guard let categoryColor = UIColor(hexString: category.color!) else {fatalError()}
                cell.backgroundColor = categoryColor
                cell.textLabel?.textColor = ContrastColorOf(categoryColor, returnFlat: true)
        }
        
        return cell
    }

    
    //MARK: - TableView Delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //trigger segue to items
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    //runs just before performing segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //if segue has identifier "" run this //in case we have more segues
        let destinationVC = segue.destination as! TodoListViewController
    
        //identify current row that is selected .indexPathfor..
        //optional,could be no row selected but we trigger segue only when we select a row
        if let indexPath = tableView.indexPathForSelectedRow {
            //property inside todolistVC, gets passed to an optional, only when its set runs {}
            destinationVC.selectedCategory = categories?[indexPath.row]
        }
    }
    

    //MARK: - Data Manipulation methods

    func save(category: Category) {
        do {
            try realm.write {
                realm.add(category)
            }
        } catch {
            print("err saving category \(error)")
        }
        
            self.tableView.reloadData() //So loadCategories() is called again
    }
    
    func loadCategories(){
        //pull all items inside realm that are of category objects in a container
            //returns Results data type, equivalent with a list data type
        categories = realm.objects(Category.self)
        tableView.reloadData() //calls all dataSource methods again
    }
    
    //MARK: - Delete Data From Swipe
    //override and call the superclass method
    
    override func updateModel(at indexPath: IndexPath) {
        if let categoryForDeletion = self.categories?[indexPath.row] {
            do {
                try self.realm.write {
                    self.realm.delete(categoryForDeletion)
                }
            } catch {
                print("Err deleting category, \(error)")
            }
        }
    }
    
    
    //MARK: - Add new categories
        
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var textField = UITextField()
        
        let alert = UIAlertController(title: "Add new category", message: "", preferredStyle: .alert)
                
        let action = UIAlertAction(title: "Add", style: .default) { (action) in
            //what happens when user clicks btn on UIAlert //closure .self
            
        //CREATE
            let newCategory = Category()
            newCategory.name = textField.text!
            
            newCategory.color =  UIColor.randomFlat().hexValue()

        //NO NEED TO APPEND, AUTO UPDATING CONTAINER TAKES CARE OF THAT
        //self.categories.append(newCategory)

            self.save(category: newCategory)
        }
        
        //once textfield is created
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Add a new category"
            textField = alertTextField
        }
        //add it to alert
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
}


            

